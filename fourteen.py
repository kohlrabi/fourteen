#!/usr/bin/env python3

import numpy as np
from typing import Union


def true_ve(days: int, ve: Union[list, np.ndarray]) -> np.ndarray:
    """Return the true VE for a piecewise list/array of (day, ve) pairs.

    Args:
        days (int): Number of days
        ve (Union[list, np.ndarray]): Tuples of piecewise days and VE values

    Returns:
        np.ndarray: Continuous true VE for every day for the values given
    """
    r = []
    ve = np.asarray(ve)

    index = -1
    for day in range(days):
        if (i := index + 1) < ve.shape[0] and day >= ve[i, 0]:
            index = i
        r.append(ve[index, 1])

    return np.array(r)


def ve_meas(ve: Union[list, np.ndarray], trick: int = 0) -> np.ndarray:
    """Returns the integrated measured VE
    Optionally apply a "invincibility" trick for `trick` number of days in the beginning

    Args:
        ve (Union[list, np.ndarray]): True VE values for each day
        trick (int, optional): Day until invincibility trick should be applied. Defaults to 0.

    Returns:
        np.ndarray: Measured cumulative VE
    """
    ve = np.asarray(ve).copy()
    if trick > 0:
        ve[:trick] = 1
    return np.cumsum(ve) / np.arange(1, ve.size + 1)


def create_study(
    days: int, trick: int, ve: Union[np.ndarray, list]
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Create a new virtual study for `days` days with invoincibility trick until day `trick`
    and ve values in `ve`
    """
    v = true_ve(days, ve)
    ve_t = ve_meas(v, trick=trick)
    ve_nt = ve_meas(v)

    return v, ve_t, ve_nt


def plot_study(days: int, v: np.ndarray, ve_t: np.ndarray, ve_nt: np.ndarray) -> None:
    """Plot a study"""
    days_x = np.arange(days)

    plt.plot(days_x, ve_nt, label=f"Without {trick}d trick", linewidth=2)

    plt.plot(days_x, ve_t, label=f"With {trick}d trick", linewidth=2)

    plt.plot(days_x, v, label=f"True VE", linewidth=2)

    plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(1.0))

    plt.grid()

    plt.xlabel("Days")

    plt.ylabel("Vaccine Effectiveness")

    plt.legend(loc=0)

    plt.title(f"Measured VE for a study of up X days")

    plt.show()


def create_and_plot_study(days: int, trick: int, ve: Union[np.ndarray, list]) -> None:
    plot_study(days, *create_study(days, trick, ve))


if __name__ == "__main__":
    """Main loop when executed from command line"""
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mtick

    # Parameters to play with
    days = 200
    trick = 14
    ve = np.array(
        ((0, -3), (trick // 2, 0), (trick, 0.3), (80, 0.2), (85, 0.1), (90, 0.0))
    )

    create_and_plot_study(days, trick, ve)
